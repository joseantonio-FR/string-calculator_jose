﻿describe("StringCalculator", function() {
    describe("1. Create a simple String calculator with a method int Add(string numbers)", function () {

        it("it should return 0 when add ''", function() {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('');

            expect(result).toEqual(0);
        });

        it("it should return 1 when add '1'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('1');

            expect(result).toEqual(1);
        });

        it("it should return 2 when add '2'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('2');

            expect(result).toEqual(2);
        });

        it("it should return 3 when add '3'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('3');

            expect(result).toEqual(3);
        });

        it("it should return 3 when add '1,2'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('1,2');

            expect(result).toEqual(3);
        });

        it("it should return 5 when add '3,2'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('3,2');

            expect(result).toEqual(5);
        });

    });

    describe("2. Allow the Add method to handle an unknown amount of numbers ", function () {

        it("it should return 6 when add '1,3,2'", function() {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('1,3,2');

            expect(result).toEqual(6);
        });
    });

    describe("3. Allow the Add method to handle new lines between numbers (instead of commas).", function () {

        it("it should return 6 when add '1\n2,3'", function () {
            var stringCalculator = new StringCalculator();
            var result = stringCalculator.add('1\n2,3');

            expect(result).toEqual(6);
        });
    });
});

describe("StringDelimiter", function() {

    it("it should return comma delimiter when add comma delimiter", function () {
        var stringDelimiter = new StringDelimiter();
        stringDelimiter.add(',');
        var delimiters = stringDelimiter.getDelimiters();

        expect(delimiters.length).toEqual(1);
        expect(delimiters[0]).toEqual(',');
        expect(stringDelimiter.getRegex()).toEqual(/,/);
    });

    it("it should return newline delimiter when add newline delimiter", function () {
        var stringDelimiter = new StringDelimiter();
        stringDelimiter.add('\n');
        var delimiters = stringDelimiter.getDelimiters();

        expect(delimiters.length).toEqual(1);
        expect(delimiters[0]).toEqual('\n');
        expect(stringDelimiter.getRegex()).toEqual(/\n/);
    });

    it("it should return comma and newline delimiters when add comma and newline delimiter", function () {
        var stringDelimiter = new StringDelimiter();
        stringDelimiter.add(',');
        stringDelimiter.add('\n');
        var delimiters = stringDelimiter.getDelimiters();

        expect(delimiters.length).toEqual(2);
        expect(delimiters[0]).toEqual(',');
        expect(delimiters[1]).toEqual('\n');
        expect(stringDelimiter.getRegex()).toEqual(/,|\n/);
    });
});