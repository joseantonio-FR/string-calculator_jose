﻿function StringDelimiter() {
    this._delimiters = "";
}


StringDelimiter.prototype.add = function (delimiter) {
    this._delimiters = this._delimiters + delimiter;
};

StringDelimiter.prototype.getDelimiters = function () {
    return this._delimiters.split('');
};

StringDelimiter.prototype.getRegex = function () {
    var regEx = "/";
    for (var i = 0; i < this._delimiters.length; i++) {
        regEx += this._delimiters[i] + "|";
    }
    regEx = regEx.substring(0, regEx.length - 1);
    regEx += "/";    
    return regEx;
};