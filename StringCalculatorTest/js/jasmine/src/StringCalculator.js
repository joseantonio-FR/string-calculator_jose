﻿function StringCalculator() {
    this._stringDelimiter = new StringDelimiter();
    this._stringDelimiter.add(',');
    this._stringDelimiter.add('\n');
}

StringCalculator.prototype.add = function (numbers) {
    if (numbers == '') return 0;
    if (!this._hasDelimiter(numbers)) return parseInt(numbers);
    return this._getTotal(numbers);
};

StringCalculator.prototype._hasDelimiter = function (numbers) {
    var delimiters = this._stringDelimiter.getDelimiters();
    for (var i = 0, len = delimiters.length; i < len; i++) {
        if (numbers.indexOf(delimiters[i]) > 0) return true;
    }
    return false;
};

StringCalculator.prototype._getTotal = function (numbers) {
    var total = 0;
    var parts = numbers.split(/,|\n/);
    for (var i = 0, len = parts.length; i < len; i++) {
        total += parseInt(parts[i]);
    }
    return total;
};