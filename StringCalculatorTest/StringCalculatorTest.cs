﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringCalculatorTest
{
    [TestClass]
    public class StringCalculatorTest
    {
        private StringCalculator _stringCalculator;

        [TestInitialize]
        public void Initialize()
        {
            _stringCalculator = new StringCalculator();
        }

        [TestMethod]
        public void ItShould_Result0_When_NoAddNumbers()
        {
            var total = _stringCalculator.Add("");
            Assert.AreEqual(0, total);
        }


        [TestMethod]
        public void ItShould_Result0_When_NumbersIsNull()
        {
            var total = _stringCalculator.Add(null);
            Assert.AreEqual(0, total);
        }

        [TestMethod]
        public void ItShould_Result1_When_NumbersIs1()
        {
            var total = _stringCalculator.Add("1");
            Assert.AreEqual(1, total);
        }

        [TestMethod]
        public void ItShould_Result2_When_NumbersIs2()
        {
            var total = _stringCalculator.Add("2");
            Assert.AreEqual(2, total);
        }

        [TestMethod]
        public void ItShould_Result1_When_NumbersIs0Comma1()
        {
            var total = _stringCalculator.Add("0,1");
            Assert.AreEqual(1, total);
        }

        [TestMethod]
        public void ItShould_Result1_When_NumbersIs1Comma0()
        {
            var total = _stringCalculator.Add("1,0");
            Assert.AreEqual(1, total);
        }

        [TestMethod]
        public void ItShould_Result2_When_NumbersIs1Comma1()
        {
            var total = _stringCalculator.Add("1,1");
            Assert.AreEqual(2, total);
        }

        [TestMethod]
        public void ItShould_Result2_When_NumbersIs0Comma2()
        {
            var total = _stringCalculator.Add("0,2");
            Assert.AreEqual(2, total);
        }

        [TestMethod]
        public void ItShould_Result2_When_NumbersIs2Comma0()
        {
            var total = _stringCalculator.Add("2,0");
            Assert.AreEqual(2, total);
        }

        [TestMethod]
        public void ItShould_Result3_When_NumbersIs0Comma1Comma2()
        {
            var total = _stringCalculator.Add("0,1,2");
            Assert.AreEqual(3, total);
        }

        [TestMethod]
        public void ItShould_CorrectResults_When_NumbersHasThreeNumbers()
        {
            Assert.AreEqual(3, _stringCalculator.Add("0,1,2"));
            Assert.AreEqual(4, _stringCalculator.Add("2,0,2"));
            Assert.AreEqual(5, _stringCalculator.Add("2,1,2"));
            Assert.AreEqual(6, _stringCalculator.Add("1,2,3"));
        }

        [TestMethod]
        public void ItShould_CorrectResults_When_NumbersHasFourNumbers()
        {
            Assert.AreEqual(4, _stringCalculator.Add("1,0,1,2"));
            Assert.AreEqual(5, _stringCalculator.Add("1,2,0,2"));
            Assert.AreEqual(6, _stringCalculator.Add("1,2,1,2"));
            Assert.AreEqual(7, _stringCalculator.Add("1,1,2,3"));
        }

        [TestMethod]
        public void ItShould_CorrectResults_When_NumbersHasFiveNumbers()
        {
            Assert.AreEqual(5, _stringCalculator.Add("1,0,1,2,1"));
            Assert.AreEqual(6, _stringCalculator.Add("1,2,0,2,1"));
            Assert.AreEqual(7, _stringCalculator.Add("1,2,1,2,1"));
            Assert.AreEqual(8, _stringCalculator.Add("1,1,2,3,1"));
        }

        [TestMethod]
        public void ItShould_Result3_When_NumbersIs1NewLine2()
        {
            var total = _stringCalculator.Add("1\n2");
            Assert.AreEqual(3, total);
        }

        [TestMethod]
        public void ItShould_Result6_When_NumbersIs1NewLine2Comma3()
        {
            var total = _stringCalculator.Add("1\n2,3");
            Assert.AreEqual(6, total);
        }

        [TestMethod]
        public void ItShould_Result3_When_NumbersHasDelimiterAtBeginnig()
        {
            var total = _stringCalculator.Add("//;\n1;2");
            Assert.AreEqual(3, total);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeException),"negatives not allowed -1")]
        public void ItShould_ResultException_When_NumbersHasANegativeValueInFirstPosition()
        {
            var total = _stringCalculator.Add("-1,2");
            Assert.AreEqual(3, total);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeException), "negatives not allowed -2")]
        public void ItShould_ResultException_When_NumbersHasANegativeValueInSecondPosition()
        {
            var total = _stringCalculator.Add("1,-2");
            Assert.AreEqual(3, total);
        }

        [TestMethod]
        [ExpectedException(typeof(NegativeException), "negatives not allowed -1,-2")]
        public void ItShould_ResultException_When_NumbersHasANegativeValueInAllPositions()
        {
            var total = _stringCalculator.Add("-1,-2");
            Assert.AreEqual(3, total);
        }
    }

    public class NegativeException : Exception
    {
        public NegativeException(string message) : base(message) { }
    }

    public class StringCalculator
    {
        private const char CHAR_EMPTY = '\0';

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers)) return 0;
            var delimiters = new[] { ',', '\n' };
            var inputDelimiter = ReadNewDelimiter(numbers);
            if (HasNewDelimiter(inputDelimiter))
            {
                delimiters = AddNewDelimiter(delimiters, inputDelimiter);
                numbers = RemoveDelimiter(numbers, inputDelimiter);
            }
            if (HasOneNumber(numbers, delimiters)) return ToInt(numbers);
            var parts = numbers.Split(delimiters);
            return GetTotal(parts);
        }

        private int GetTotal(string[] parts)
        {
            var total = 0;
            var negativeValues = GetNegativeValues(parts);
            if (negativeValues.Count>0)
            {
                throw new NegativeException("negatives not allowed " + negativeValues.ToArray());
            }
            foreach (var number in parts)
            {
                total += ToInt(number);
            }
            return total;
        }

        private List<int> GetNegativeValues(string[] parts)
        {
            var negatives = new List<int>();
            foreach (var number in parts)
            {
                var intNumber = ToInt(number);
                if (intNumber < 0) { negatives.Add(intNumber); }
            }
            return negatives;
        }

        private static bool HasNewDelimiter(char inputDelimiter)
        {
            return !inputDelimiter.Equals(CHAR_EMPTY);
        }

        private static string RemoveDelimiter(string numbers, char inputDelimiter)
        {
            return numbers.Replace("//" + inputDelimiter + "\n", "");
        }

        private static char[] AddNewDelimiter(char[] delimiters, char inputDelimiter)
        {
            return delimiters.Concat(new[] {inputDelimiter}).ToArray();
        }

        private char ReadNewDelimiter(string numbers)
        {
            if (numbers.Length<5) return CHAR_EMPTY;
            if (numbers.Substring(0, 2) != "//") return CHAR_EMPTY;
            return char.Parse(numbers.Substring(2, 1));
        }

        private int ToInt(string number)
        {
            return Int16.Parse(number);
        }

        private bool HasOneNumber(string numbers, char[] delimiters)
        {
            return delimiters.All(delimiter => !numbers.Contains(delimiter));
        }
    }
}
